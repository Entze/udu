package dev.entze.udu.common.data.musicalNotation

import kotlin.test.assertEquals
import kotlin.test.Test


internal class MusicalKeyTestkt {

    /**
     * Ugly monster test
     */
    @Test
    fun getMusicalKeyDistanceHalfsteps_CES() {
        assertEquals(MusicalKey.H, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(12))
        assertEquals(MusicalKey.H, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(12))
        assertEquals(MusicalKey.H, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(12))
        assertEquals(MusicalKey.CES, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(0))
        assertEquals(MusicalKey.CES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(0))
        assertEquals(MusicalKey.CES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(0))
        assertEquals(MusicalKey.H, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-12))
        assertEquals(MusicalKey.H, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-12))
        assertEquals(MusicalKey.H, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-12))
        assertEquals(MusicalKey.C, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(1))
        assertEquals(MusicalKey.C, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(1))
        assertEquals(MusicalKey.C, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(1))
        assertEquals(MusicalKey.C, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-11))
        assertEquals(MusicalKey.C, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-11))
        assertEquals(MusicalKey.C, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-11))
        assertEquals(MusicalKey.CIS, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(2))
        assertEquals(MusicalKey.CIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(2))
        assertEquals(MusicalKey.DES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(2))
        assertEquals(MusicalKey.DES, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-10))
        assertEquals(MusicalKey.CIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-10))
        assertEquals(MusicalKey.DES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-10))
        assertEquals(MusicalKey.D, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(3))
        assertEquals(MusicalKey.D, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(3))
        assertEquals(MusicalKey.D, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(3))
        assertEquals(MusicalKey.D, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-9))
        assertEquals(MusicalKey.D, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-9))
        assertEquals(MusicalKey.D, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-9))
        assertEquals(MusicalKey.DIS, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(4))
        assertEquals(MusicalKey.DIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(4))
        assertEquals(MusicalKey.ES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(4))
        assertEquals(MusicalKey.ES, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-8))
        assertEquals(MusicalKey.DIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-8))
        assertEquals(MusicalKey.ES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-8))
        assertEquals(MusicalKey.E, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(5))
        assertEquals(MusicalKey.E, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(5))
        assertEquals(MusicalKey.E, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(5))
        assertEquals(MusicalKey.E, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-7))
        assertEquals(MusicalKey.E, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-7))
        assertEquals(MusicalKey.E, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-7))
        assertEquals(MusicalKey.F, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(6))
        assertEquals(MusicalKey.F, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(6))
        assertEquals(MusicalKey.F, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(6))
        assertEquals(MusicalKey.F, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-6))
        assertEquals(MusicalKey.F, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-6))
        assertEquals(MusicalKey.F, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-6))
        assertEquals(MusicalKey.FIS, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(7))
        assertEquals(MusicalKey.FIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(7))
        assertEquals(MusicalKey.GES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(7))
        assertEquals(MusicalKey.GES, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-5))
        assertEquals(MusicalKey.FIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-5))
        assertEquals(MusicalKey.GES, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-5))
        assertEquals(MusicalKey.G, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(8))
        assertEquals(MusicalKey.G, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(8))
        assertEquals(MusicalKey.G, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(8))
        assertEquals(MusicalKey.G, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-4))
        assertEquals(MusicalKey.G, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-4))
        assertEquals(MusicalKey.G, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-4))
        assertEquals(MusicalKey.GIS, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(9))
        assertEquals(MusicalKey.GIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(9))
        assertEquals(MusicalKey.AS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(9))
        assertEquals(MusicalKey.AS, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-3))
        assertEquals(MusicalKey.GIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-3))
        assertEquals(MusicalKey.AS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-3))
        assertEquals(MusicalKey.A, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(10))
        assertEquals(MusicalKey.A, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(10))
        assertEquals(MusicalKey.A, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(10))
        assertEquals(MusicalKey.A, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-2))
        assertEquals(MusicalKey.A, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-2))
        assertEquals(MusicalKey.A, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-2))
        assertEquals(MusicalKey.AIS, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(11))
        assertEquals(MusicalKey.AIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(11))
        assertEquals(MusicalKey.B, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(11))
        assertEquals(MusicalKey.B, MusicalKey.CES.getMusicalKeyDistanceHalfsteps(-1))
        assertEquals(MusicalKey.AIS, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferSharp(-1))
        assertEquals(MusicalKey.B, MusicalKey.CES.getMusicalKeyDistanceHalfstepsPreferFlat(-1))
    }

    @Test
    fun test_getClassicalStringRepresentation_naturalNote_E() {
        assertEquals("E", MusicalKey.E.getClassicalStringRepresentation())
    }

    @Test
    fun test_getClassicalStringRepresentation_naturalNote_H() {
        assertEquals("H", MusicalKey.H.getClassicalStringRepresentation())
    }

    @Test
    fun test_getClassicalStringRepresentation_Sharp_AIS() {
        assertEquals("A♯", MusicalKey.AIS.getClassicalStringRepresentation())
    }

    @Test
    fun test_getClassicalStringRepresentation_Sharp_HIS() {
        assertEquals("H♯", MusicalKey.HIS.getClassicalStringRepresentation())
    }

    @Test
    fun test_getClassicalStringRepresentation_Flat_DES() {
        assertEquals("D♭", MusicalKey.DES.getClassicalStringRepresentation())
    }

    @Test
    fun test_getClassicalStringRepresentation_Flat_B() {
        assertEquals("B", MusicalKey.B.getClassicalStringRepresentation())
    }


    @Test
    fun test_getAnglosaxonStringRepresentation_naturalNote_E() {
        assertEquals("E", MusicalKey.E.getAngloSaxonStringRepresentation())
    }

    @Test
    fun test_getAnglosaxonStringRepresentation_naturalNote_H() {
        assertEquals("B", MusicalKey.H.getAngloSaxonStringRepresentation())
    }

    @Test
    fun test_getAnglosaxonStringRepresentation_Sharp_AIS() {
        assertEquals("A♯", MusicalKey.AIS.getAngloSaxonStringRepresentation())
    }

    @Test
    fun test_getAnglosaxonStringRepresentation_Sharp_HIS() {
        assertEquals("B♯", MusicalKey.HIS.getAngloSaxonStringRepresentation())
    }

    @Test
    fun test_getAnglosaxonStringRepresentation_Flat_DES() {
        assertEquals("D♭", MusicalKey.DES.getAngloSaxonStringRepresentation())
    }

    @Test
    fun test_getAnglosaxonStringRepresentation_Flat_B() {
        assertEquals("B♭", MusicalKey.B.getAngloSaxonStringRepresentation())
    }


    @Test
    fun test_getID3StringRepresentation_naturalNote_E() {
        assertEquals("E", MusicalKey.E.getID3StringRepresentation())
    }

    @Test
    fun test_getID3StringRepresentation_naturalNote_H() {
        assertEquals("B", MusicalKey.H.getID3StringRepresentation())
    }

    @Test
    fun test_getID3StringRepresentation_Sharp_AIS() {
        assertEquals("A#", MusicalKey.AIS.getID3StringRepresentation())
    }

    @Test
    fun test_getID3StringRepresentation_Sharp_HIS() {
        assertEquals("B#", MusicalKey.HIS.getID3StringRepresentation())
    }

    @Test
    fun test_getID3StringRepresentation_Flat_DES() {
        assertEquals("Db", MusicalKey.DES.getID3StringRepresentation())
    }

    @Test
    fun test_getID3StringRepresentation_Flat_B() {
        assertEquals("Bb", MusicalKey.B.getID3StringRepresentation())
    }

    @Test
    fun test_getAlphanumericStringRepresentation_Flat_B() {
        assertEquals("11", MusicalKey.B.getAlphanumericStringRepresentation())
    }


}