package dev.entze.udu.rekordbox.repository

import dev.entze.udu.rekordbox.dao.*
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.Test

class RekordboxRepositoryTest {

    private val testFile = File("src/test/resources/test.xml")
    private val expectedProduct = RekordboxProduct(name = "rekordbox", version = "5.6.0", company = "Pioneer DJ")
    private val expectedTracks = listOf(
        RekordboxTrack(
            trackID = 1,
            name = "NOISE",
            artist = "",
            composer = "",
            album = "",
            grouping = "",
            genre = "",
            kind = "WAV File",
            size = 1382226L,
            totalTime = 5.0,
            discNumber = 0,
            trackNumber = 0,
            year = 0,
            averageBPM = 0.00,
            dateModified = "",
            dateAdded = "2019-05-30",
            bitRate = 2116,
            sampleRate = 44100.0,
            comments = "",
            playCount = 0,
            lastPlayed = "",
            rating = 0,
            location = "file://localhost{testLibraryDirectory}/PioneerDJ/Sampler/OSC_SAMPLER/PRESET%20ONESHOT/NOISE.wav",
            remixer = "",
            tonality = "",
            label = "",
            mix = "",
            colour = "",
            tempos = emptyList(),
            positionMarks = emptyList()
        ),
        RekordboxTrack(
            trackID = 2,
            name = "SINEWAVE",
            artist = "",
            composer = "",
            album = "",
            grouping = "",
            genre = "",
            kind = "WAV File",
            size = 1515258L,
            totalTime = 5.0,
            discNumber = 0,
            trackNumber = 0,
            year = 0,
            averageBPM = 0.00,
            dateModified = "",
            dateAdded = "2019-05-30",
            bitRate = 2116,
            sampleRate = 44100.0,
            comments = "",
            playCount = 0,
            lastPlayed = "",
            rating = 0,
            location = "file://localhost{testLibraryDirectory}/PioneerDJ/Sampler/OSC_SAMPLER/PRESET%20ONESHOT/SINEWAVE.wav",
            remixer = "",
            tonality = "",
            label = "",
            mix = "",
            colour = "",
            tempos = emptyList(),
            positionMarks = emptyList()
        ),
        RekordboxTrack(
            trackID = 3,
            name = "SIREN",
            artist = "",
            composer = "",
            album = "",
            grouping = "",
            genre = "",
            kind = "WAV File",
            size = 1941204L,
            totalTime = 7.0,
            discNumber = 0,
            trackNumber = 0,
            year = 0,
            averageBPM = 0.00,
            dateModified = "",
            dateAdded = "2019-05-30",
            bitRate = 2116,
            sampleRate = 44100.0,
            comments = "",
            playCount = 0,
            lastPlayed = "",
            rating = 0,
            location = "file://localhost{testLibraryDirectory}/PioneerDJ/Sampler/OSC_SAMPLER/PRESET%20ONESHOT/SIREN.wav",
            remixer = "",
            tonality = "",
            label = "",
            mix = "",
            colour = "",
            tempos = emptyList(),
            positionMarks = emptyList()
        ),
        RekordboxTrack(
            trackID = 4,
            name = "HORN",
            artist = "",
            composer = "",
            album = "",
            grouping = "",
            genre = "",
            kind = "WAV File",
            size = 2010816L,
            totalTime = 7.0,
            discNumber = 0,
            trackNumber = 0,
            year = 0,
            averageBPM = 0.00,
            dateModified = "",
            dateAdded = "2019-05-30",
            bitRate = 2116,
            sampleRate = 44100.0,
            comments = "",
            playCount = 0,
            lastPlayed = "",
            rating = 0,
            location = "file://localhost{testLibraryDirectory}/PioneerDJ/Sampler/OSC_SAMPLER/PRESET%20ONESHOT/HORN.wav",
            remixer = "",
            tonality = "",
            label = "",
            mix = "",
            colour = "",
            tempos = emptyList(),
            positionMarks = emptyList()
        ),
        RekordboxTrack(
            trackID = 5,
            name = "Demo Track 1",
            artist = "Loopmasters",
            composer = "",
            album = "",
            grouping = "",
            genre = "",
            kind = "MP3 File",
            size = 6899624L,
            totalTime = 172.0,
            discNumber = 0,
            trackNumber = 0,
            year = 0,
            averageBPM = 128.00,
            dateModified = "",
            dateAdded = "2019-05-30",
            bitRate = 320,
            sampleRate = 44100.0,
            comments = "Tracks by www.loopmasters.com",
            playCount = 0,
            lastPlayed = "",
            rating = 0,
            location = "file://localhost{testLibraryDirectory}/PioneerDJ/Demo%20Tracks/Demo%20Track%201.mp3",
            remixer = "",
            tonality = "",
            label = "Loopmasters",
            mix = "",
            colour = "",
            tempos = listOf(RekordboxTempo(0.025, 128.00, "4/4", battito = 1)),
            positionMarks = listOf(
                RekordboxPositionMark(
                    name = "",
                    type = 4,
                    start = 15.025,
                    end = 22.525,
                    num = -1,
                    red = null, green = null, blue = null
                ),
                RekordboxPositionMark(
                    name = "",
                    type = 4,
                    start = 26.275,
                    end = 28.150,
                    num = -1,
                    red = null, green = null, blue = null
                ),
                RekordboxPositionMark(
                    name = "",
                    type = 0,
                    start = 35.650,
                    end = null,
                    num = -1,
                    red = null, green = null, blue = null
                ),
                RekordboxPositionMark(
                    name = "",
                    type = 0,
                    start = 36.119,
                    end = null,
                    num = -1,
                    red = null, green = null, blue = null
                ),
                RekordboxPositionMark(
                    name = "",
                    type = 4,
                    start = 37.525,
                    end = 38.463,
                    num = 0,
                    red = 255, green = 140, blue = 0
                ),
                RekordboxPositionMark(
                    name = "",
                    type = 4,
                    start = 41.275,
                    end = 45.025,
                    num = 1,
                    red = 222, green = 68, blue = 207
                ),
                RekordboxPositionMark(
                    name = "",
                    type = 0,
                    start = 65.650,
                    end = null,
                    num = 2,
                    red = 40, green = 226, blue = 20
                ),
                RekordboxPositionMark(
                    name = "",
                    type = 0,
                    start = 37.525,
                    end = 38.463,
                    num = 0,
                    red = 255, green = 140, blue = 0
                )

            )
        ),
        RekordboxTrack(
            trackID = 6,
            name = "Demo Track 2",
            artist = "Loopmasters",
            composer = "",
            album = "",
            grouping = "",
            genre = "",
            kind = "MP3 File",
            size = 5124342L,
            totalTime = 128.0,
            discNumber = 0,
            trackNumber = 0,
            year = 0,
            averageBPM = 120.00,
            dateModified = "",
            dateAdded = "2019-05-30",
            bitRate = 320,
            sampleRate = 44100.0,
            comments = "Tracks by www.loopmasters.com",
            playCount = 0,
            lastPlayed = "",
            rating = 0,
            location = "file://localhost{testLibraryDirectory}/PioneerDJ/Demo%20Tracks/Demo%20Track%202.mp3",
            remixer = "",
            tonality = "",
            label = "Loopmasters",
            mix = "",
            colour = "",
            tempos = listOf(
                RekordboxTempo(0.025, 120.00, "4/4", battito = 1),
                RekordboxTempo(48.026, 120.00, "4/4", battito = 1),
                RekordboxTempo(48.525, 120.00, "4/4", battito = 2),
                RekordboxTempo(49.026, 120.00, "4/4", battito = 3),
                RekordboxTempo(49.525, 120.00, "4/4", battito = 4),
                RekordboxTempo(50.026, 120.00, "4/4", battito = 1),
                RekordboxTempo(50.525, 120.00, "4/4", battito = 2),
                RekordboxTempo(51.026, 120.00, "4/4", battito = 3),
                RekordboxTempo(51.525, 120.00, "4/4", battito = 4),
                RekordboxTempo(52.026, 120.00, "4/4", battito = 1)
            ),
            positionMarks = emptyList()
        )
    )
    private val expectedCollection = RekordboxCollection(entries = 7, tracks = expectedTracks)
    private val expectedPlaylists = RekordboxPlaylist(
        listOf(
            RekordboxNode(
                type = 0,
                name = "ROOT",
                count = 3,
                entries = null,
                keyType = null,
                tracks = null,
                nodes = listOf(
                    RekordboxNode(
                        type = 1,
                        name = "Intelligent Playlist",
                        count = null,
                        keyType = 0,
                        entries = 0,
                        tracks = emptyList(),
                        nodes = emptyList()
                    ),
                    RekordboxNode(
                        type = 1,
                        name = "Regular Playlist",
                        count = null,
                        keyType = 0,
                        entries = 0,
                        tracks = emptyList(),
                        nodes = emptyList()
                    ),
                    RekordboxNode(
                        type = 0,
                        name = "Playlist Folder",
                        count = 4,
                        keyType = 0,
                        entries = 0,
                        tracks = null,
                        nodes = listOf(
                            RekordboxNode(
                                name = "Test",
                                type = 1,
                                keyType = 0,
                                entries = 1,
                                count = null,
                                tracks = listOf(RekordboxNodeTrack("1")),
                                nodes = emptyList()
                            ),
                            RekordboxNode(
                                name = "Playlist Folder",
                                type = 0,
                                count = 2,
                                keyType = null,
                                entries = null,
                                tracks = null,
                                nodes = listOf(
                                    RekordboxNode(
                                        name = "Test",
                                        type = 1,
                                        keyType = 0,
                                        entries = 1,
                                        tracks = listOf(RekordboxNodeTrack("1")),
                                        count = null,
                                        nodes = emptyList()
                                    )
                                )
                            ),
                            RekordboxNode(
                                name = "Nested Playlist Folder",
                                type = 0,
                                count = 2,
                                keyType = null,
                                entries = null,
                                tracks = null,
                                nodes = listOf(
                                    RekordboxNode(
                                        name = "Test",
                                        type = 1,
                                        keyType = 0,
                                        entries = 1,
                                        tracks = listOf(RekordboxNodeTrack("3")),
                                        nodes = emptyList(),
                                        count = null
                                    ),
                                    RekordboxNode(
                                        name = "Another Nested Playlist Folder",
                                        type = 0,
                                        count = 2,
                                        keyType = null,
                                        entries = null,
                                        tracks = null,
                                        nodes = listOf(
                                            RekordboxNode(
                                                name = "Test",
                                                type = 1,
                                                keyType = 0,
                                                entries = 1,
                                                tracks = listOf(RekordboxNodeTrack("3")),
                                                nodes = emptyList(),
                                                count = null
                                            ),
                                            RekordboxNode(
                                                name = "Test",
                                                type = 1,
                                                keyType = 0,
                                                entries = 1,
                                                tracks = listOf(RekordboxNodeTrack("2")),
                                                nodes = emptyList(),
                                                count = null
                                            )
                                        )
                                    )
                                )
                            ),
                            RekordboxNode(
                                name = "Loopmasters",
                                type = 1,
                                keyType = 0,
                                entries = 2,
                                count = null,
                                tracks = listOf(RekordboxNodeTrack("5"), RekordboxNodeTrack("6")),
                                nodes = emptyList()
                            )

                        )
                    )
                )
            )
        )
    )
    private val expectedDao = RekordboxDAO(
        version = "1.0.0",
        product = expectedProduct,
        collection = expectedCollection,
        playlists = expectedPlaylists
    )

    @Test
    fun test_readsWholeRepository() {
        val rekordboxRepository = RekordboxRepository(testFile)

        val dao = rekordboxRepository.extractData()
        assertEquals(expectedDao, dao)
    }

}