package dev.entze.udu.rekordbox.dao

data class RekordboxNode(
    val type: Int,
    val name: String,
    val count: Int?,
    val entries: Int?,
    val keyType: Int?,
    val tracks: List<RekordboxNodeTrack>?,
    val nodes: List<RekordboxNode>
) {

}
