package dev.entze.udu.rekordbox.dao

data class RekordboxCollection(
    val entries: Int,
    val tracks: List<RekordboxTrack>
) {

}
