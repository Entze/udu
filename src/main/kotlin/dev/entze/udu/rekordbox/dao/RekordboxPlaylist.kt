package dev.entze.udu.rekordbox.dao

data class RekordboxPlaylist(
    val nodes: List<RekordboxNode>
) {

}
