package dev.entze.udu.rekordbox.dao

data class RekordboxProduct(
    val name: String,
    val version: String,
    val company: String
) {

}
