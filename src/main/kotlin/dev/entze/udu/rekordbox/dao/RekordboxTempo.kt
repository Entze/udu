package dev.entze.udu.rekordbox.dao

data class RekordboxTempo(
    val inizio: Double, //in Second (with decimal numbers)
    val bpm: Double, //in Seconds (with decimal numbers)
    val metro: String, //3/4, 4/4 etc.
    val battito: Int //depending on Metro
) {

}
