package dev.entze.udu.rekordbox.dao

data class RekordboxPositionMark(
    val name: String,
    val type: Int, //Cue = "0", Fade-In = "1", Fade-Out = "2", Load = "3",  Loop = "4"
    val start: Double, //in Seconds (with decimal numbers)
    val end: Double?, //in Seconds (with decimal numbers)
    val num: Int, //HotCue A = 0, B = 1,... ; Memory Cue: -1
    val red: Short?,
    val green: Short?,
    val blue: Short?
) {

}
