package dev.entze.udu.rekordbox.dao

data class RekordboxTrack(
    val trackID: Int,
    val name: String,
    val artist: String,
    val composer: String,
    val album: String,
    val grouping: String,
    val genre: String,
    val kind: String,
    val size: Long, //in Octet
    val totalTime: Double, //in Seconds (without decimal numbers)
    val discNumber: Int,
    val trackNumber: Int,
    val year: Int,
    val averageBPM: Double, //in Seconds (with decimal numbers)
    val dateModified: String, //in yyyy-mm-dd
    val dateAdded: String, //in yyyy-mm-dd
    val bitRate: Int, //in Kbps
    val sampleRate: Double, // in Hertz
    val comments: String,
    val playCount: Int,
    val lastPlayed: String, //in yyyy-mm-dd
    val rating: Int, //0 star = "0", 1 star = "51", 2 stars = "102", 3 stars = "153", 4 stars = "204", 5 stars = "255"
    val location: String, //URI
    val remixer: String,
    val tonality: String,
    val label: String,
    val mix: String,
    val colour: String, // 0xRRBBGG
    val tempos: List<RekordboxTempo>,
    val positionMarks: List<RekordboxPositionMark>
)