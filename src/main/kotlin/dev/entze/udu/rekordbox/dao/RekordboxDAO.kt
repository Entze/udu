package dev.entze.udu.rekordbox.dao

import dev.entze.udu.common.DataAccessObject
import dev.entze.udu.common.UniversalCollection

data class RekordboxDAO(
    val version: String,
    val product: RekordboxProduct,
    val collection: RekordboxCollection,
    val playlists: RekordboxPlaylist


) : DataAccessObject {

    override fun extractCollection(): UniversalCollection {
        TODO("Not yet implemented")
    }

}