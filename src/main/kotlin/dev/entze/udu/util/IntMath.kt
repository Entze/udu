package dev.entze.udu.util

fun Int.modulo(other: Int): Int {
    if (other == 0) {
        throw ArithmeticException("/ by zero")
    }
    if (this == 0) {
        return 0
    }

    var mod = this.rem(other)
    if (mod == 0) {
        return 0
    }

    if ((this < 0).xor(other < 0)) {
        mod += other
    }

    return mod
}