package dev.entze.udu.common.data.musicalNotation

enum class MusicalScale {
    DUR,
    MOLL
}