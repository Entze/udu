package dev.entze.udu.common.data.musicalNotation


class Tonality(val key: MusicalKey = MusicalKey.C, val scale: MusicalScale = MusicalScale.DUR) {

}