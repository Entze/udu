package dev.entze.udu.common.data.musicalNotation

import dev.entze.udu.common.data.musicalNotation.MusicalKey.*
import dev.entze.udu.util.modulo
import kotlin.ArithmeticException

private val sharpPriority: Array<MusicalKey> =
    arrayOf(C, D, E, F, E, G, A, H, CIS, DIS, EIS, FIS, GIS, AIS, HIS, CES, DES, ES, FES, GES, AS, B)
private val flatPriority: Array<MusicalKey> =
    arrayOf(C, D, E, F, E, G, A, H, CES, DES, ES, FES, GES, AS, B, CIS, DIS, EIS, FIS, GIS, AIS, HIS)

enum class MusicalKey(val musicalOrdinal: Int = 0) {

    CES(11), C(0), CIS(1),
    DES(1), D(2), DIS(3),
    ES(3), E(4), EIS(5),
    FES(4), F(5), FIS(6),
    GES(6), G(7), GIS(8),
    AS(8), A(9), AIS(10),
    B(10), H(11), HIS(0);

    /**
     * Returns true iff other equals the musical key in chromatic notation.
     * @param other - the other key
     * @return true iff other equals the musical key in chromatic notation.
     */
    fun musicalEquals(other: MusicalKey): Boolean {
        return this.musicalOrdinal == other.musicalOrdinal
    }

    /**
     * Returns the first MusicalKey that is halfsteps away. 0 returns itself. Positive prefers sharps negative prefers flats.
     * @param halfsteps - default 1: distance in halfsteps
     * @return the first MusicalKey that is halfsteps away.
     */
    fun getMusicalKeyDistanceHalfsteps(halfsteps: Int = 1): MusicalKey {
        if (halfsteps >= 0) {
            return getMusicalKeyDistanceHalfstepsPreferSharp(halfsteps)
        }
        return getMusicalKeyDistanceHalfstepsPreferFlat(halfsteps)

    }


    /**
     * Returns the MusicalKey that is halfsteps away. 0 returns itself. If both a sharp and a flat exists it returns the sharp.
     * @param halfsteps - default 1: distance in halfsteps
     * @return the first MusicalKey that is halfsteps away preferring sharps if necessary.
     */
    fun getMusicalKeyDistanceHalfstepsPreferSharp(halfsteps: Int = 1): MusicalKey {
        if (halfsteps == 0) {
            return this
        }
        val targetMusicalOrdinal: Int = (this.musicalOrdinal + halfsteps).modulo(12)
        return sharpPriority.find { k -> (k.musicalOrdinal == targetMusicalOrdinal) }!!
    }

    /**
     * Returns the MusicalKey that is halfsteps away. 0 returns itself. If both a sharp and a flat exists it returns the flat.
     * @param halfsteps - default 1: distance in halfsteps
     * @return the first MusicalKey that is halfsteps away preferring flats if necessary.
     */
    fun getMusicalKeyDistanceHalfstepsPreferFlat(halfsteps: Int = 1): MusicalKey {
        if (halfsteps == 0) {
            return this
        }
        val targetMusicalOrdinal: Int = (this.musicalOrdinal + halfsteps).modulo(12)
        return flatPriority.find { k -> (k.musicalOrdinal == targetMusicalOrdinal) }!!
    }

    /**
     * Returns a string of the key in classical (German) notation.
     * @return classical (German) notation of the key.
     */
    fun getClassicalStringRepresentation(): String {
        if (this.name.length > 1) {
            val firstChar: Char = this.name.first()
            if (this.name.endsWith("IS")) {
                return "${firstChar}♯"
            }
            return "${firstChar}♭"
        }
        return this.name
    }

    /**
     * Returns a string of the key in classical (Anglo Saxon) notation.
     * @return classical (Anglo Saxon) notation of the key.
     */
    fun getAngloSaxonStringRepresentation(): String {
        return when {
            this == B -> {
                "B♭"
            }
            this == H -> {
                "B"
            }
            this == HIS -> {
                "B♯"
            }
            else -> getClassicalStringRepresentation()
        }
    }

    /**
     * Returns a string of the key according to the ID3 notation.
     * @return ID3 notation of the key.
     */
    fun getID3StringRepresentation(): String {
        return when {
            this == B -> {
                "Bb"
            }
            this == H -> {
                "B"
            }
            this == HIS -> {
                "B#"
            }
            this.name.length > 1 -> {
                val firstChar: Char = this.name.first()
                if (this.name.endsWith(("IS"))) {
                    "${firstChar}#"
                } else {
                    "${firstChar}b"
                }
            }
            else -> this.name
        }
    }

    /**
     * Returns the key in alphanumeric notation
     */
    fun getAlphanumericStringRepresentation(shift: Int = 0, rescale: Int = 7, offset: Int = 1): String {
        if (rescale != 1 && rescale != 5 && rescale != 7 && rescale != 11) {
            throw ArithmeticException("Cannot rescale with factor $rescale as it is not unique. Use 1, 5, 7 (default), or 11.")
        }
        return ((this.musicalOrdinal * rescale + shift).modulo(12) + offset).toString()
    }

    companion object Factory {

        fun musicalKeyFromAlphanumeric(key: Int, shift: Int = 0, rescale: Int = 7, offset: Int = 1): MusicalKey {
            if (rescale != 1 && rescale != 5 && rescale != 7 && rescale != 11) {
                throw ArithmeticException("Cannot rescale from factor $rescale as it is not unique. Use 1, 5, 7 (default), or 11.")
            } else if (rescale == 1 || rescale == 7) {
                return C.getMusicalKeyDistanceHalfstepsPreferSharp((key - offset - shift) * rescale)
            }
            return C.getMusicalKeyDistanceHalfstepsPreferFlat((key - offset - shift) * rescale)
        }

        fun musicalKeyFromAlphanumeric(key: String, shift: Int = 0, rescale: Int = 7, offset: Int = 1): MusicalKey {
            return musicalKeyFromAlphanumeric(key.toInt(), shift, rescale, offset)
        }
    }

}


