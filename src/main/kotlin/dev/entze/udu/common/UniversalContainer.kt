package dev.entze.udu.common

data class UniversalContainer(
    val internalId: Long,
    val trackIds: List<Long>?,
    val inheritTracks: Boolean,
    val passDownTracks: Boolean
) {

}
