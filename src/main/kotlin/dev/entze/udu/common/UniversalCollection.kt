package dev.entze.udu.common

data class UniversalCollection(
    val tracks: Set<UniversalTrack>,
    val container: Set<UniversalContainer>,
    val hierarchy: UniversalHierarchy
) {
}