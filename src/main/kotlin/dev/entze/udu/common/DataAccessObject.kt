package dev.entze.udu.common

interface DataAccessObject {

    fun extractCollection(): UniversalCollection

}