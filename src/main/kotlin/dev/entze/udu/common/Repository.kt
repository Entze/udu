package dev.entze.udu.common

interface Repository<T> where T : DataAccessObject {

    fun extractData(): T

    fun insertData(dataAccessObject: T): Unit

}